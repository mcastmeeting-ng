#include <stdlib.h>
#include <check.h>
#include "libmcast.h"
#include "err.h"

/* test data */
char *addr_ok = "239.1.1.1";
uint32_t port_ok = 9999;
char *addr_ok2 = "239.1.1.2";
uint32_t port_ok2 = 9998;
uint32_t ttl_ok = 1;
uint32_t loop_ok = 1;

char *addr_noaddr = "this is no addr";
char *addr_nomcast = "192.168.0.1";

START_TEST (test_mcast_join_ok_single)
{
    /* join */
    err_status_t result;
    result = mcast_join_group(addr_ok, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 1);
    /* leave */
    result = mcast_leave_group(addr_ok, port_ok);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 0);
}
END_TEST

START_TEST (test_mcast_join_ok_double)
{
    /* join */
    err_status_t result;
    result = mcast_join_group(addr_ok, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_ok);
    result = mcast_join_group(addr_ok2, port_ok2, ttl_ok, loop_ok);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 2);
    /* leave */
    result = mcast_leave_group(addr_ok, port_ok);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 1);
    result = mcast_leave_group(addr_ok2, port_ok2);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 0);
}
END_TEST

START_TEST (test_mcast_join_bad_double)
{
    /* join */
    err_status_t result;
    result = mcast_join_group(addr_ok, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_ok);
    result = mcast_join_group(addr_ok, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_fail);
    fail_unless(mcast_group_count(0) == 1);
    /* leave */
    result = mcast_leave_group(addr_ok, port_ok);
    fail_unless(result == err_status_ok);
    fail_unless(mcast_group_count(0) == 0);
}
END_TEST

START_TEST (test_mcast_join_noaddr)
{
    err_status_t result;
    result = mcast_join_group(addr_noaddr, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_bad_addr);

}
END_TEST

START_TEST (test_mcast_join_nomcastaddr)
{
    err_status_t result;
    result = mcast_join_group(addr_nomcast, port_ok, ttl_ok, loop_ok);
    fail_unless(result == err_status_bad_addr);
}
END_TEST

Suite *
libmcast_suite (void)
{
    Suite *s = suite_create("libmcast");
    /* core test cases */
    TCase *tc_core = tcase_create ("Core");
    tcase_add_test (tc_core, test_mcast_join_ok_single);
    tcase_add_test (tc_core, test_mcast_join_ok_double);
    tcase_add_test (tc_core, test_mcast_join_bad_double);
    tcase_add_test (tc_core, test_mcast_join_noaddr);
    tcase_add_test (tc_core, test_mcast_join_nomcastaddr);
    suite_add_tcase (s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s = libmcast_suite ();
    SRunner *sr = srunner_create (s);
    srunner_run_all (sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed (sr);
    srunner_free (sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

