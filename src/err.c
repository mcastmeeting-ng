/*
** Error reporting functions
**
** Thanks to David A. McGrew from Cisco Systems
** for ideas and source code
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "err.h"
#include "config.h"

#ifdef ERR_REPORTING_SYSLOG
# ifdef HAVE_SYSLOG_H
#  include <syslog.h>
# endif
#endif

/*  err_level reflects the level of errors that are reported  */
err_reporting_level_t err_level = err_level_none;

/* err_file is the FILE to which errors are reported */
static FILE *err_file = NULL;

err_status_t
err_reporting_init(char *ident)
{

#ifdef ERR_REPORTING_SYSLOG
	openlog(ident, LOG_PID, LOG_AUTHPRIV);
#endif

	/*
	 * Believe it or not, openlog doesn't return an error on failure.
	 * But then, neither does the syslog() call...
	 */

#ifdef ERR_REPORTING_STDOUT
	err_file = stdout;
#elif defined(USE_ERR_REPORTING_FILE)
	/* open file for error reporting */
	err_file = fopen(ERR_REPORTING_FILE, "w");
	if (err_file == NULL)
		return err_status_init_fail;
#endif

	return err_status_ok;
}

void
err_report(int priority, char *format, ...)
{

	va_list args;

	if (priority <= err_level) {
		va_start(args, format);
		if (err_file != NULL) {
			vfprintf(err_file, format, args);
			/*      fprintf(err_file, "\n"); */
		}
#ifdef ERR_REPORTING_SYSLOG
		if (1) { /* FIXME: Make this a runtime option. */
			int syslogpri;

			switch (priority) {
				case err_level_emergency:
					syslogpri = LOG_EMERG;
					break;
				case err_level_alert:
					syslogpri = LOG_ALERT;
					break;
				case err_level_critical:
					syslogpri = LOG_CRIT;
					break;
				case err_level_error:
					syslogpri = LOG_ERR;
					break;
				case err_level_warning:
					syslogpri = LOG_WARNING;
					break;
				case err_level_notice:
					syslogpri = LOG_NOTICE;
					break;
				case err_level_info:
					syslogpri = LOG_INFO;
					break;
				case err_level_debug:
				case err_level_none:
				default:
					syslogpri = LOG_DEBUG;
					break;
			}

			vsyslog(syslogpri, format, args);
		}
#endif
			va_end(args);
	}
}

void
err_reporting_set_level(err_reporting_level_t lvl)
{
	err_level = lvl;
}

