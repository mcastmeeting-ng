/*
** Multicast Helper Library Implementation
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <errno.h>

#include "libmcast.h"

/**
 * the debug module of the library
 */
debug_module_t mod_libmcast = {
    1,                  /* debug is on by default for now */
    "libmcast"          /* printable name of module */
};

/**
 * the linked list with group information
 */
mcast_group_t *group_list;

/* forward declarations */
err_status_t mcast_add_group(struct sockaddr_in *mc_group,
        struct ip_mreq *mreq, uint32_t sock, uint32_t port,
        uint32_t ttl, uint32_t loop);
err_status_t mcast_remove_group(char *mcast_addr, uint32_t port);
void mcast_free(mcast_group_t **mcast_group);

/**
 * join a specific multicast group
 *
 * @param mcast_addr    multicast address
 * @param port          port to use
 * @param ttl           ttl value
 * @param loop          loop to local?
 * @return err_status_ok on success
 */
err_status_t
mcast_join_group(char *mcast_addr, uint32_t port,
        uint32_t ttl, uint32_t loop)
{
    int s;
    struct sockaddr_in *mc_group = NULL;
    struct ip_mreq *mreq = NULL;

    /* check if already joined */
    if (mcast_get_group(mcast_addr, port) != NULL) {
        debug_print2(mod_libmcast, "already joined to %s:%d",
                mcast_addr, port);
        return err_status_fail;
    }
    /* allocate memory */
    mc_group = calloc(1, sizeof(struct sockaddr_in));
    if (mc_group == NULL) {
        return err_status_alloc_fail;
    }
    mreq = calloc(1, sizeof(struct ip_mreq));
    if (mreq == NULL) {
        return err_status_alloc_fail;
    }
    /* ipv4 address check first */
    if (inet_aton(mcast_addr, &mc_group->sin_addr) == 0) {
        debug_print(mod_libmcast, "no valid IPv4 addr received: %s",
                mcast_addr);
        return err_status_bad_addr;
    }
    if (mc_group->sin_addr.s_addr == INADDR_NONE) {
        debug_print(mod_libmcast, "address error: %s", mcast_addr);
        return err_status_bad_addr;
    }
    /* multicast addr check */
    if (!ADDR_IS_MULTICAST(mc_group->sin_addr.s_addr)) {
        debug_print(mod_libmcast, "no valid mcast address: %s", mcast_addr);
        return err_status_bad_addr;
    }

    mc_group->sin_family = AF_INET;
    mc_group->sin_port = htons(port);

    /* create socket */
    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s < 0) {
        int err;
        err = errno;
        debug_print(mod_libmcast, "could not open socket %d", err);
        return err_status_socket_err;
    }

    /* set TTL value to ttl specified */
    if (setsockopt(s, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
        debug_print(mod_libmcast, "could not set TTL value: %d", ttl);
        /* non-critical */
    }

    /* join new mcast group, we let the kernel decide wich interface to use */
    mreq->imr_multiaddr.s_addr = mc_group->sin_addr.s_addr;
    mreq->imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(s, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                (void*)mreq, sizeof(struct ip_mreq)) < 0) {
        debug_print(mod_libmcast, "could not join mcast group: %s", mcast_addr);
        return err_status_mcast_join;
    }
    /* add this group to linked list */
    if (mcast_add_group(mc_group, mreq, s, port, ttl, loop) != err_status_ok) {
        debug_print(mod_libmcast, "error adding group %s to list", mcast_addr);
        return err_status_fail;
    }

    return err_status_ok;
}

/**
 * leave a specific mcast group
 * @param mcast_addr    mcast addr for lookup
 * @param port          port of mcast group
 * @return err_status_ok on success, appropriate error messages
 *         on failure
 */
err_status_t
mcast_leave_group(char *mcast_addr, uint32_t port)
{
    mcast_group_t *grp = NULL;
    grp = mcast_get_group(mcast_addr, port);
    if (grp == NULL) {
        debug_print2(mod_libmcast, "no group for %s:%d found",
                mcast_addr, port);
        return err_status_bad_param;
    }
    /* leave group */
    int ret;
    ret = setsockopt(grp->socket, IPPROTO_IP, IP_DROP_MEMBERSHIP,
            grp->mreq, sizeof(struct ip_mreq));
    if (ret < 0) {
        debug_print2(mod_libmcast, "could not leave group %s:%d",
                mcast_addr, port);
        return err_status_fail;
    }
    /* remove from list */
    return mcast_remove_group(mcast_addr, port);
}
/**
 * add a new mcast group to linked list
 *
 * @param mc_group      sockadr info for this group
 * @param mreq          ip_mreq info for this group
 * @param sock          corresponding socker
 * @param port          port of group
 * @param ttl           ttl of group
 * @param loop          loop settings
 * @return err_status_ok on success, err_status_alloc_fail on error
 */
err_status_t
mcast_add_group(struct sockaddr_in *mc_group, struct ip_mreq *mreq,
        uint32_t sock, uint32_t port, uint32_t ttl, uint32_t loop)
{
    mcast_group_t *mg;
    /* allocate memory first */
    mg = calloc(1, sizeof(mcast_group_t));
    if (mg == NULL) {
        return err_status_alloc_fail;
    }
    /* set fields */
    mg->addr = mc_group;
    mg->mreq = mreq;
    mg->socket = sock;
    mg->port = port;
    mg->ttl = ttl;
    mg->loop = loop;

    /* is it the first element ? */
    if (group_list == NULL) {
        group_list = mg;
        return err_status_ok;
    } else {
        mg->next = group_list;
        group_list = mg;
    }
    return err_status_ok;
}

/**
 * remove a group from linked group list
 *
 * @param mcast_addr    mcast addr of group to remove
 * @param port          port of group to remove
 * @return err_status_ok on success, err_status_fail on failure
 */
err_status_t
mcast_remove_group(char *mcast_addr, uint32_t port)
{
    /* TODO: make it smoother */
    /* get pointer to group */
    mcast_group_t *remove_group = NULL;
    remove_group = mcast_get_group(mcast_addr, port);
    if (remove_group == NULL) {
        debug_print2(mod_libmcast, "no group %s:%d found for removal",
                mcast_addr, port);
        return err_status_fail;
    }
    /* only one group in list? */
    if (mcast_group_count(0) == 1) {
        mcast_free(&group_list);
        return err_status_ok;
    }
    /* is it the first group? */
    if (remove_group == group_list) {
        mcast_group_t *tmp;
        tmp = group_list;
        mcast_free(&tmp);
        group_list = group_list->next;
    }
    /* else: find prev. group */
    mcast_group_t *prev;
    prev = group_list;
    while (prev != NULL) {
        if (prev->next == remove_group)
            break;
        prev = prev->next;
    }

    prev->next = remove_group->next;
    mcast_free(&remove_group);

    return err_status_ok;
}

/**
 * free memory of removed group
 *
 * @param mcast_group   pointer to group
 */
void
mcast_free(mcast_group_t **mcast_group)
{
    free((*mcast_group)->addr);
    free((*mcast_group)->mreq);
    free(*mcast_group);
    *mcast_group = NULL;
}

/**
 * get specific mcast group identified by addr and port
 *
 * @param mcast_addr    mcast addr to use for lookup
 * @param port          port to use for lookup
 * @return mcast group matching search criteria or NULL if
 *         no mcast group matching the search criteria was found
 */
mcast_group_t*
mcast_get_group(char *mcast_addr, uint32_t port)
{
    mcast_group_t *found = NULL;
    mcast_group_t *grp;
    /* walk down the list */
    grp = group_list;
    while (grp != NULL) {
        if (grp->port == port) {
            if (strncmp(mcast_addr,
                        inet_ntoa(grp->addr->sin_addr), 16) == 0) {
                found = grp;
                break;
            }
        }
        grp = grp->next;
    }

    return found;
}

/**
 * get group count and output debug information
 *
 * @param output        also write debug info to stdout
 * @return number of managed groups
 */
uint32_t mcast_group_count(uint32_t output)
{
    int c = 0;
    mcast_group_t *grp;
    grp = group_list;
    while (grp != NULL) {
        if (output) {
            printf("* mcast_grp [%d]:%s\n", c,
                    inet_ntoa(grp->addr->sin_addr));
            printf("  (port: %d, ttl: %d, loop:%d)\n",
                    grp->port, grp->ttl, grp->loop);
        }
        grp = grp->next;
        c++;
    }
    return c;
}

