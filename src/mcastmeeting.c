#include <config.h>
#include <stdio.h>
#include <stdlib.h>

#include "err.h"	/* error codes */

debug_module_t mod_mcastmeeting = {
	1,				/* debugging is enabled for now */
	"mcastmeeting"	/* printable name of module */
};

int main (int argc, char **argv)
{
	err_status_t status;
	/* init error reporting system */
	status = err_reporting_init("mcastmeeting");
	if (status) {
		fprintf(stderr, "could not init error reporting\n");
		return EXIT_FAILURE;
	}

	debug_print(mod_mcastmeeting, "** %s starting up", PACKAGE_STRING);

	return EXIT_SUCCESS;
}

