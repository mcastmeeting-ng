/*
** Multicast Helper Library
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef LIBMCAST_H
#define LIBMCAST_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "config.h"
#include "err.h"

/**
 * mcast_group_t saves all information of a particular multicast group
 */
typedef struct mcast_group {
    struct sockaddr_in *addr;   /**< multicast group addr info         */
    struct ip_mreq *mreq;       /**< mreq struct needed for operations */
    int socket;                 /**< assigned socket                   */
    uint32_t port;              /**< port of group                     */
    uint32_t ttl;               /**< ttl of mcast group                */
    uint32_t loop;              /**< whether to local loop or not      */
    struct mcast_group *next;   /**< pointer to next element in list   */
} mcast_group_t;

/* functions */
err_status_t mcast_join_group(char *mcast_addr, uint32_t port,
        uint32_t ttl, uint32_t loop);
mcast_group_t* mcast_get_group(char *mcast_addr, uint32_t port);

/* debug functions */
uint32_t mcast_group_count(uint32_t output);

/**
 * mcast_addr check macro
 */
#define ADDR_IS_MULTICAST(a) IN_MULTICAST(htonl(a))

#endif /* LIBMCAST_H */

