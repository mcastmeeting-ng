<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>config.h</name>
    <path>/tmp/mcastmeeting/</path>
    <filename>config_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>ENABLE_DEBUGGING</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>2f114d6efb488275e5911e57d34ef946</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ERR_REPORTING_STDOUT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>022495a49acfe6687ae1a3a4733ee44c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_ARPA_INET_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>7d1a71461e07569d0c9003da24c30a59</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_DLFCN_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>0ee1617ff2f6885ef384a3dd46f9b9d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_INET_ATON</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>d719da5884fb483b57d9d1ccdea9ec10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_INTTYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>b90a030ff2790ebdc176660a6dd2a478</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_MALLOC</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>14503280ca0cb757db915eea09282bfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_MEMORY_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>e93a78f9d076138897af441c9f86f285</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_NETINET_IN_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>0938658b988a082864efcee2d1b2bfd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SOCKET</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>4cd6234bfe3f7c10968a1151f43ce280</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDINT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>b6cd6d1c63c1e26ea2d4537b77148354</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STDLIB_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>9e0e434ec1a6ddbd97db12b5a32905e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRINGS_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>405d10d46190bcb0320524c54eafc850</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_STRING_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>d4c234dd1625255dc626a15886306e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYSLOG_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>695d4e005378df4a1f286cb3d0f4b333</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_SOCKET_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>fb96c2bc08ebf33cca68c714f624a58d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_STAT_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ce156430ba007d19b4348a950d0c692b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_SYS_TYPES_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>69dc70bea5d1f8bd2be9740e974fa666</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_UNISTD_H</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>219b06937831d0da94d801ab13987639</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>HAVE_USLEEP</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>176348b5cb3250d8116ad36bf163c024</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>ca8570fb706c81df371b7f9bc454ae03</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_BUGREPORT</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>1d1d2d7f8d2f95b376954d649ab03233</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_NAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>1c0439e4355794c09b64274849eb0279</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_STRING</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>c73e6f903c16eca7710f92e36e1c6fbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_TARNAME</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>f415af6bfede0e8d5453708afe68651c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PACKAGE_VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>a326a05d5e30f9e9a4bb0b4469d5d0c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STDC_HEADERS</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>550e5c272cc3cf3814651721167dcd23</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>VERSION</name>
      <anchorfile>config_8h.html</anchorfile>
      <anchor>1c6d5de492ac61ad29aec7aa9a436bbf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>err.h</name>
    <path>/tmp/mcastmeeting/include/</path>
    <filename>err_8h</filename>
    <class kind="struct">debug_module_t</class>
    <member kind="define">
      <type>#define</type>
      <name>debug_print</name>
      <anchorfile>err_8h.html</anchorfile>
      <anchor>413bb4b31aaf1c1a04496ab0d07f2fa9</anchor>
      <arglist>(mod, format, arg)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>debug_on</name>
      <anchorfile>err_8h.html</anchorfile>
      <anchor>0b0e611aaf5b4113d87401dfc3386f88</anchor>
      <arglist>(mod)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>debug_off</name>
      <anchorfile>err_8h.html</anchorfile>
      <anchor>75bb021f8bcc06340ae27b363dd61c8a</anchor>
      <arglist>(mod)</arglist>
    </member>
    <member kind="enumeration">
      <name>err_status_t</name>
      <anchor>g494f2de0f45bd38d26dd3211000d65dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_ok</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd20f367664d3ed77d63ea43c626c94776</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dde2cbf6df389f8f3f46ccca1d274f0be5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_bad_param</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd0bbcfd0e448e8ab7d3037b456d30d787</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_alloc_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd333adc414d8b32821f76072f2665378d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_dealloc_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddf5891d2f626f97b0e56f35c72cbbfa8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_init_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddcfaf9662487141fd7ff49cb8705fae5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_terminus</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd1c63a177c4c6531e341281d06fb1854e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_auth_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddc278e7211d5e2faca6a37a6404dba070</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_cipher_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd2882694f28997e0777b60cfce145cfa9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_replay_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd72493b7e1ad9fd32648a503b5f21b66b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_replay_old</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7437715143d4ae5cd9ce359ada97c947</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_algo_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd19d41a635ddfb75657073fff1c080627</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_no_such_op</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd8a6d0379a5f516fc0f4a4a269435cffe</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_no_ctx</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd9f090f384e41741ac7977a3ee6244e87</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_cant_check</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd8e10e4aa944ed0427c133e158c2607f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_key_expired</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dddbff52c0ef6c2ed52f40191483015cf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_socket_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddf0d66ca49d0a87c9d63620dd88acc84d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_signal_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7c0fc9c90c46c545e91e9f47d8ef46d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_nonce_bad</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd06bef06eb4fa4ceb29ce645d4b58b961</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_read_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd3902d74778b5e9f4c8ea87d3887e7f86</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_write_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd13be65e8ceb888305f85f7276ad8d132</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_parse_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd77ce890b49823f20edcc1deef88b88f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_encode_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7248bd61d2de0971cce2d3ca7e41547f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_semaphore_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddcf3bf2987002641d332087df8bffef71</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_pfkey_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd499abc5eae0d110b8aba0df878eda90a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_bad_addr</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd5b2492e524730a7cf2681f136ff7b003</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_mcast_join</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddb739c87f537b2a628ebce0ba64d53e54</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>err_reporting_level_t</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_emergency</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e940d34f39bdd166853c3f43296e2d6fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_alert</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e03fced6e1aaf7b0f024577fcada07962</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_critical</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e3d03e7d04d59214d4442723148bc85ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_error</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453ed3b0e1a06e9d7a8be82e4b51dd3589eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_warning</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e4efc67f2ef38becb0574098e03d8da55</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_notice</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453ee9e335f317d802aff6620be9bdc3cd0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_info</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e7d3278f07c98f4501889ec7c794ea0ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_debug</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453ec3c83d726d76574aa9bb11eb07feef5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_level_none</name>
      <anchor>9f80bbb7da54b77fc66e38a84b65453e18fd7a232bdf2ad1b4c3b270c0967858</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>err_status_t</type>
      <name>err_reporting_init</name>
      <anchorfile>err_8h.html</anchorfile>
      <anchor>046bd1e7bce9f3c3e248bcfec3c19bee</anchor>
      <arglist>(char *ident)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>err_report</name>
      <anchorfile>err_8h.html</anchorfile>
      <anchor>e2ef8a54880df53f630a51fbea144fcd</anchor>
      <arglist>(int priority, char *format,...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>libmcast.h</name>
    <path>/tmp/mcastmeeting/include/</path>
    <filename>libmcast_8h</filename>
    <includes id="config_8h" name="config.h" local="yes" imported="no">config.h</includes>
    <includes id="err_8h" name="err.h" local="yes" imported="no">err.h</includes>
    <class kind="struct">mcast_group</class>
    <member kind="define">
      <type>#define</type>
      <name>ADDR_IS_MULTICAST</name>
      <anchorfile>libmcast_8h.html</anchorfile>
      <anchor>f9fc5062a74ffea6afb75ff3634f9cf6</anchor>
      <arglist>(a)</arglist>
    </member>
    <member kind="typedef">
      <type>mcast_group</type>
      <name>mcast_group_t</name>
      <anchorfile>libmcast_8h.html</anchorfile>
      <anchor>f5554958e668cd37534d86c1ae1bb0b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>err_status_t</type>
      <name>mcast_join_group</name>
      <anchorfile>libmcast_8h.html</anchorfile>
      <anchor>74e85bb2cfc35716c5860df4e96bc271</anchor>
      <arglist>(char *mcast_addr, uint32_t port, uint32_t ttl, uint32_t loop)</arglist>
    </member>
    <member kind="function">
      <type>mcast_group_t *</type>
      <name>mcast_get_group</name>
      <anchorfile>libmcast_8h.html</anchorfile>
      <anchor>a6c7f9a633452ddee92aac519282342a</anchor>
      <arglist>(char *mcast_addr, uint32_t port)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>mcast_group_count</name>
      <anchorfile>libmcast_8h.html</anchorfile>
      <anchor>f8c8d3ae32f801fe04d3d08db938be5c</anchor>
      <arglist>(uint32_t output)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>libmcast.c</name>
    <path>/tmp/mcastmeeting/lib/</path>
    <filename>libmcast_8c</filename>
    <includes id="libmcast_8h" name="libmcast.h" local="yes" imported="no">libmcast.h</includes>
    <member kind="function">
      <type>err_status_t</type>
      <name>mcast_add_group</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>03620e091a78dec226c7c8121289aebd</anchor>
      <arglist>(struct sockaddr_in *mc_group, struct ip_mreq *mreq, uint32_t sock, uint32_t port, uint32_t ttl, uint32_t loop)</arglist>
    </member>
    <member kind="function">
      <type>err_status_t</type>
      <name>mcast_remove_group</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>9845ef02308f6e1cccb5e4241c02b182</anchor>
      <arglist>(char *mcast_addr, uint32_t port)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>mcast_free</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>dfa778b9a780f2c5ab3c98b83459560a</anchor>
      <arglist>(mcast_group_t **mcast_group)</arglist>
    </member>
    <member kind="function">
      <type>err_status_t</type>
      <name>mcast_join_group</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>74e85bb2cfc35716c5860df4e96bc271</anchor>
      <arglist>(char *mcast_addr, uint32_t port, uint32_t ttl, uint32_t loop)</arglist>
    </member>
    <member kind="function">
      <type>err_status_t</type>
      <name>mcast_leave_group</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>0bbfcb97da4cec91e3f38ce432285ca6</anchor>
      <arglist>(char *mcast_addr, uint32_t port)</arglist>
    </member>
    <member kind="function">
      <type>mcast_group_t *</type>
      <name>mcast_get_group</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>a6c7f9a633452ddee92aac519282342a</anchor>
      <arglist>(char *mcast_addr, uint32_t port)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>mcast_group_count</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>f8c8d3ae32f801fe04d3d08db938be5c</anchor>
      <arglist>(uint32_t output)</arglist>
    </member>
    <member kind="variable">
      <type>debug_module_t</type>
      <name>mod_libmcast</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>f090f11f215f1c1cb897076cdd181782</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>mcast_group_t *</type>
      <name>group_list</name>
      <anchorfile>libmcast_8c.html</anchorfile>
      <anchor>96c2707ea6d1242bb2d0bdb57c9b206e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>err.c</name>
    <path>/tmp/mcastmeeting/src/</path>
    <filename>err_8c</filename>
    <includes id="err_8h" name="err.h" local="yes" imported="no">err.h</includes>
    <includes id="config_8h" name="config.h" local="yes" imported="no">config.h</includes>
    <member kind="function">
      <type>err_status_t</type>
      <name>err_reporting_init</name>
      <anchorfile>err_8c.html</anchorfile>
      <anchor>046bd1e7bce9f3c3e248bcfec3c19bee</anchor>
      <arglist>(char *ident)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>err_report</name>
      <anchorfile>err_8c.html</anchorfile>
      <anchor>e2ef8a54880df53f630a51fbea144fcd</anchor>
      <arglist>(int priority, char *format,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>err_reporting_set_level</name>
      <anchorfile>err_8c.html</anchorfile>
      <anchor>5168c407a2ae7c7fa9621b074b7392c1</anchor>
      <arglist>(err_reporting_level_t lvl)</arglist>
    </member>
    <member kind="variable">
      <type>err_reporting_level_t</type>
      <name>err_level</name>
      <anchorfile>err_8c.html</anchorfile>
      <anchor>28562972c41a98380ca64a7729980df2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>mcastmeeting.c</name>
    <path>/tmp/mcastmeeting/src/</path>
    <filename>mcastmeeting_8c</filename>
    <includes id="config_8h" name="config.h" local="no" imported="no">config.h</includes>
    <includes id="err_8h" name="err.h" local="yes" imported="no">err.h</includes>
    <member kind="function">
      <type>int</type>
      <name>main</name>
      <anchorfile>mcastmeeting_8c.html</anchorfile>
      <anchor>3c04138a5bfe5d72780bb7e82a18e627</anchor>
      <arglist>(int argc, char **argv)</arglist>
    </member>
    <member kind="variable">
      <type>debug_module_t</type>
      <name>mod_mcastmeeting</name>
      <anchorfile>mcastmeeting_8c.html</anchorfile>
      <anchor>92f2f19c0c8e259061595aa3ca748df6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>check_libmcast.c</name>
    <path>/tmp/mcastmeeting/tests/</path>
    <filename>check__libmcast_8c</filename>
    <includes id="libmcast_8h" name="libmcast.h" local="yes" imported="no">libmcast.h</includes>
    <includes id="err_8h" name="err.h" local="yes" imported="no">err.h</includes>
    <member kind="variable">
      <type>char *</type>
      <name>addr_ok</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>7381b28ad18aa45f651876c13f1a710f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>port_ok</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>69d4f636113fe037c639dff8e7beed96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>addr_ok2</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>c55462fd719ae2c4e709938e55c67055</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>port_ok2</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>f0e238c3e2a132d10e2fd800adad927e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ttl_ok</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>96befd2e134edff2031a281f969be314</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>loop_ok</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>d3ddaf6fe453ebb1de601de9ed777ffa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>addr_noaddr</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>6d5282ce17dd5f6b60ace817b563ec29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>addr_nomcast</name>
      <anchorfile>check__libmcast_8c.html</anchorfile>
      <anchor>c9f9047a355cc957a2860340a24952fc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>Error</name>
    <title>Error Codes</title>
    <filename>group___error.html</filename>
    <member kind="enumeration">
      <name>err_status_t</name>
      <anchor>g494f2de0f45bd38d26dd3211000d65dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_ok</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd20f367664d3ed77d63ea43c626c94776</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dde2cbf6df389f8f3f46ccca1d274f0be5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_bad_param</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd0bbcfd0e448e8ab7d3037b456d30d787</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_alloc_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd333adc414d8b32821f76072f2665378d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_dealloc_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddf5891d2f626f97b0e56f35c72cbbfa8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_init_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddcfaf9662487141fd7ff49cb8705fae5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_terminus</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd1c63a177c4c6531e341281d06fb1854e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_auth_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddc278e7211d5e2faca6a37a6404dba070</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_cipher_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd2882694f28997e0777b60cfce145cfa9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_replay_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd72493b7e1ad9fd32648a503b5f21b66b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_replay_old</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7437715143d4ae5cd9ce359ada97c947</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_algo_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd19d41a635ddfb75657073fff1c080627</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_no_such_op</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd8a6d0379a5f516fc0f4a4a269435cffe</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_no_ctx</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd9f090f384e41741ac7977a3ee6244e87</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_cant_check</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd8e10e4aa944ed0427c133e158c2607f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_key_expired</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dddbff52c0ef6c2ed52f40191483015cf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_socket_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddf0d66ca49d0a87c9d63620dd88acc84d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_signal_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7c0fc9c90c46c545e91e9f47d8ef46d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_nonce_bad</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd06bef06eb4fa4ceb29ce645d4b58b961</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_read_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd3902d74778b5e9f4c8ea87d3887e7f86</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_write_fail</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd13be65e8ceb888305f85f7276ad8d132</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_parse_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd77ce890b49823f20edcc1deef88b88f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_encode_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd7248bd61d2de0971cce2d3ca7e41547f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_semaphore_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddcf3bf2987002641d332087df8bffef71</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_pfkey_err</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd499abc5eae0d110b8aba0df878eda90a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_bad_addr</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65dd5b2492e524730a7cf2681f136ff7b003</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>err_status_mcast_join</name>
      <anchor>gg494f2de0f45bd38d26dd3211000d65ddb739c87f537b2a628ebce0ba64d53e54</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>debug_module_t</name>
    <filename>structdebug__module__t.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>on</name>
      <anchorfile>structdebug__module__t.html</anchorfile>
      <anchor>76f5b3dffb24469ca2ab8dcd48c5f300</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>name</name>
      <anchorfile>structdebug__module__t.html</anchorfile>
      <anchor>7975f4b4b83bada2ae0618d9b2e2231b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>mcast_group</name>
    <filename>structmcast__group.html</filename>
    <member kind="variable">
      <type>sockaddr_in *</type>
      <name>addr</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>4d8ec21aaee5c5e7243ddc16a52e608e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ip_mreq *</type>
      <name>mreq</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>a830de211880fdd92b96aebc20a7d7bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>socket</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>32baa6f6c7fd9966724191894268cf6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>port</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>c7619d4b3495aefc9427edcac78c3e8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ttl</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>c0659aa5f2118db861dd0bb384dec853</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>loop</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>181a589056a8e128fe1d2ea8e7582b30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>mcast_group *</type>
      <name>next</name>
      <anchorfile>structmcast__group.html</anchorfile>
      <anchor>6d5db31764f8362a02cb55dbae16c423</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>
